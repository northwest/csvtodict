#!/usr/bin/env python
def GetHeaders(file, separator=","):
	returnable = {}
	fileData = open(file, "r").readlines()
	for column in fileData[0].split(separator):
		returnable[column] = len(returnable)
	return returnable, fileData


def Process(file):
	return CsvToDict(file)

def CsvToDict(file, separator=","):
	returnable = {}
	properties, fileData = GetHeaders(file, separator=separator)
	for x in fileData:
		record = {}
		for column in properties:
			record[column] = x.split(separator)[properties[column]]
		returnable[len(returnable)] = record
	return returnable



def DictToCsv(nDict, outputPath, separator=","):
	newCsv = open(outputPath, "a+")
	for x in nDict:
		line = ""
		for item in nDict[x]:
			line += str(item).replace("\r","").replace("\n","") + separator
		newCsv.write(line + "\n")
	newCsv.close()

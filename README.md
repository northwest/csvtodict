# csvToDict - python 2/3

I made this little script to convert CSV data into a usable python dict.

For example use of this script, please see

### Example

```
import csvToDict

newDict = csvToDict.Process("testData.py")

for record in newDict:
	# record is an integer, indicating the position in the dict(Array style, starting at zero)
	# The first record/row in the dict contains the headers

	# Print every value under the column "t1"
	firstColumn = newDict[record]["t1"]
	print(firstColumn)

```

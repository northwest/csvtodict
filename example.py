#!/usr/bin/env python
import sys
import csvToDict

if __name__ == "__main__":
	results = csvToDict.Process(sys.argv[1])

	print("# of records(rows): ", len(results))

	for record in results:
		print (results[record])

	csvToDict.DictToCsv(results, "output.csv")
